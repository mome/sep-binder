# SEP-Binder

Joines entries from the Stanford Encyclopedia of Philosophy into one pdf-file.

# Dependencies

* pandoc
* wget
* pandocfilters (e.g. install with pip)
* texlive / xelatex
* noto fonts
* rsvg-convert (on Ubuntu install librsvg2-bin, to render svg-files)

For non-latin unicode characters and mathematical formulas you may need some additional texlive packages.

# Usage

To list the names of all availabel articles just execute the script without arguements:

```sh
./sep-binder
```

Use grep to find filter article names:

```sh
./sep-binder | grep frege
```

```
> frege
> frege-hilbert
> frege-logic
> frege-theorem
```

Call the script and append every sep entry as an argument in the order you want them in the pdf.

```sh
./sep-binder kant idealism
```

The script will output a `sep.pdf`.
