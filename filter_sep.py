#!/usr/bin/env python3

from pandocfilters import toJSONFilter, Str, Para
import sys

def caps(key, value, format, meta):
    if key == "Div":
        if value[0][0] == "toc":
            # print(value, file=sys.stderr)
            pass
        if value[0][0] in [
                "header-wrapper" ,"article-sidebar", "article-banner", "footer",
                "academic-tools", "other-internet-resources", "related-entries", "toc"
            ]:
            return Para([Str("")])


if __name__ == "__main__":
    toJSONFilter(caps)
