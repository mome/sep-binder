#!/usr/bin/env python3

"""
Parses the mathjax configuration and modifies the latex template.
"""

import sys
import re
import json5

import csv

newcommands = []


newcommands.append(r"\usepackage{newunicodechar}")

with open('uc2tex.csv') as csvfile:
    reader = csv.reader(csvfile)
    u2t = list(reader)

for row in u2t[100:]:
    uc_sym, tex_sym, *_ = row
    if uc_sym in " ́ ̀ ̂ ̃ ̄ ̅ ̆":
        continue
    if not tex_sym:
        continue
    line = r"\newunicodechar{" + uc_sym + r"}{\ensuremath{" + tex_sym + "}}"
    newcommands.append(line)

newcommands.append("")


for path in sys.argv[1:]:
    with open(path) as f:
        lines = f.readlines()

    lines[0] = lines[0].split(" = ")[-1]
    lines[-1] = lines[-1][:-2]
    json_code = "".join(lines)
    cfg_d = json5.loads(json_code)
    if "TeX" not in cfg_d:
        #print(f"No 'TeX' in {path}", file=sys.stderr)
        continue
    if "Macros" not in cfg_d["TeX"]:
        #print(f"No 'TeX/Macros' in {path}", file=sys.stderr)
        continue
    cfg_d = cfg_d["TeX"]["Macros"]

    for key, val in cfg_d.items():
        if isinstance(val, str):
            cmd = "\\newcommand{\\" + key + "}{" + val + "}"
        elif isinstance(val, list):
            def_, num_args = val
            cmd = "\\newcommand{\\" + key + "}" + f"[{num_args}]"  + def_
        else:
            raise Exception(f"arg type is {type(val)}")
        newcommands.append(cmd)

newcommands = '\n'.join(newcommands)

with open("sep.latex") as f:
    blocks = f.read().split("\n\n")

for i, blk in enumerate(blocks):
    if r"\begin{document}" in blk:
        break

new_tex = "\n\n".join((*blocks[:i], newcommands, *blocks[i:]))

with open("new.latex", "w") as f:
    f.write(new_tex)

